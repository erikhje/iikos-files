#include <stdio.h>    /* printf */
#include <sys/wait.h> /* wait */
#include <time.h>     /* wait */
#include <unistd.h>   /* fork,sleep */

int main(void) {
	int p;

	p = fork();
	if (p == 0) { /* sjekk om vi er i child eller parent */
		printf("...er i child og skal sove 4 sekunder!\n");
		nanosleep((const struct timespec[]){{4, 0}}, NULL);
	} else {
		printf("...har skapt child og venter på at den skal avslutte!\n");
		while(wait(NULL)>0) { /*no-op*/ ; }
		printf("...og der var den ferdig, gitt.\n\n");
	}

	return 0;
}

