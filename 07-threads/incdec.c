#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>
#include "common.h"
#include "common_threads.h"

int g_ant=0;
int g_max;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

void *IncThread() {
    int i;
		Pthread_mutex_lock(&lock);
    for (i = 0; i < g_max; i++) {
    	g_ant++;
    }
		Pthread_mutex_unlock(&lock);
    return 0;
}

void *DecThread() {
    int i;
		Pthread_mutex_lock(&lock);
    for (i = 0; i < g_max; i++) {
    	g_ant--;
    }
		Pthread_mutex_unlock(&lock);
    return 0;
}


int main(int argc, char *argv[]) {                    
    if (argc != 2) {
        fprintf(stderr, "usage: ./a.out <loopcount>\n");
        exit(1);
    }
    g_max = atoi(argv[1]);
    pthread_t tid1, tid2;
    Pthread_create(&tid1, NULL, IncThread, NULL); /* start tråder */
    Pthread_create(&tid2, NULL, DecThread, NULL);
    Pthread_join(tid1, NULL);                     /* vent på tråder */
    Pthread_join(tid2, NULL); 

    if (g_ant != 0) {
        printf(" HUFF! g_ant er %d, skulle vært 0\n", g_ant);
    } else {
        printf(" OK! g_ant er %d\n", g_ant);
    }
    return 0;
}








