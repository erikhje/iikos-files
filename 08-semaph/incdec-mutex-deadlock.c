// bør legge inn en printf i trådene slik at de ikke blir så ofte deadlock, 
// nå blir det deadlock alle ganger bortsett fra når en tråd får kjøre ferdig helt første gang

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

#define NITER 1000

struct ab {
        int a;
        int b;
};

struct ab A={0,0};
pthread_mutex_t amutex,bmutex;

void *IncThread(void *a)
{
    int i;
    for (i = 0; i < NITER; i++) {
       pthread_mutex_lock(&amutex);
       pthread_mutex_lock(&bmutex);
       A.a++;
       A.b++;
       pthread_mutex_unlock(&amutex);
       pthread_mutex_unlock(&bmutex);
    }
    return 0;
}

void *DecThread(void *a)
{
    int i;
    for (i = 0; i < NITER; i++) {
       pthread_mutex_lock(&amutex);
       pthread_mutex_lock(&bmutex);
       A.a--;
       A.b--;
       pthread_mutex_unlock(&bmutex);
       pthread_mutex_unlock(&amutex);
    }
    return 0;
}

int main(int argc, char * argv[])
{
    pthread_t tid1, tid2;
    pthread_mutex_init(&amutex,0);
    pthread_mutex_init(&bmutex,0);
    pthread_create(&tid1, NULL, IncThread, NULL); /* start tråder */
    pthread_create(&tid2, NULL, DecThread, NULL);
    pthread_join(tid1, NULL);                     /* vent på tråder */
    pthread_join(tid2, NULL); 
    printf("A.a is %d, A.b is %d\n", A.a, A.b);
    return 0;
}


