#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#define SIZE 40000

int g_x[SIZE][SIZE];

typedef struct {
  int a;
} myarg_t;

void *incthread(void *arg) {
  myarg_t *args = (myarg_t *) arg;
  int start = args->a;
  int i,j;
  for (i = start; i < (start + 10000); i++) {
    for (j = 0; j < SIZE; j++) {
      g_x[i][j] = g_x[j][i] * 1;
    }
  }
  printf("i is %d, j is %d\n",i,j);
  return NULL;
}

int main(void) {
  pthread_t tid[4];
  myarg_t arg[4] = { 0, 10000, 20000, 30000 };
  for (int i = 0; i < 4; i++) {
    int rc = pthread_create(&tid[i], NULL, incthread, &arg[i]);
    assert(rc == 0);
  }
  for (int i = 0; i < 4; i++) {
    pthread_join(tid[i], NULL);
  }
  return 0;
}
